﻿---
title:   > 
  \textsc{Pic 10b: Intermediate Programming}
author:  Ricardo Salazar
date:    Winter 2018 (Lec 3)
header-includes:
  - \usepackage{caption}
---

## About the `.pdf` version of this syllabus

If you are reading the online [markdown] version of this document,
[click here][pdf] to access the location where the `.pdf` version is hosted;
then click on the `view raw` link to download a copy of this document.[^if-you]

[pdf]: readme.md.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.


## Course information

**Contact:** [`rsalazar@math.ucla.edu`][correo] (write "Pic 10B" in the
subject).

**Time:** 2:00 to 2:50 pm.

**Location:** MS 6627.

**Office hours:** [See class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |
|:---------:|:--------------|:--------:|:-------------------------------------|
| 3A        | Dobner, Alexander. |MS 3919   | [`adobner@math.ucla.edu`][t-a-1] |
| 3B        | Oakley, William. |MS 6161   | [`wgoakley@math.ucla.edu`][t-a-2] |

[correo]: mailto:rsalazar@math.ucla.edu
[class-website]: https://ccle.ucla.edu/course/view/18W-COMPTNG10B-3
[t-a-1]: mailto:adobner@math.ucla.edu
[t-a-2]: mailto:wgoakley@math.ucla.edu


## Course Description

Abstract data types and their implementation using `C++` class mechanism;
dynamic data structures, including _linked lists, stacks, queues, trees,_ and
_hash tables_; applications; object-oriented programming and software reuse;
recursion; algorithms for sorting and searching.

## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`, _2nd
Edition._ John Wiley and Sons, Inc. However, be advised that some of the
material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## CCLE and MyUCLA

This course will use a password-protected Internet site on UCLA CCLE to post
course materials and announcements. These materials can include the syllabus,
handouts and Internet links referenced in class (hard copies of said materials
**will not** be made available in class). _Please consult CCLE and this
syllabus before sending me an email about a policy or procedure; your
question(s) might already be answered here._ In addition, you can check your
scores via the MyUCLA Gradebook. Please notice that it is your responsibility
to verify in a timely manner that the scores appearing on MyUCLA are accurate.

## Midterms

Two fifty-minute midterm will be given on
**Friday, February 2**,
and 
**Friday, March 2**,
from
**2:00 to 2:50 pm**
at
**MS 6627**.
_There will be absolutely no makeup midterms under any circumstances._

## Final Exam

The final exam will be given on
**Monday, March 19**
from
**11:30 to 2:30 pm**
at
**TBD** (click [here][final-tbd] for up-to-date information about the exam
location). **_Failure to take the final exam during this time will result in
an automatic F!_**

[final-tbd]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=18W&subj_area_cd=COMPTNG&crs_catlg_no=0010B+++&class_id=157051220&class_no=+003++

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam.
Please turn off and put away any electronic devices during the entire duration
of the exam.
 
The midterm exams will be returned to you during discussion section and your TA
will go over the exam at that time. _Any questions regarding how the exam was
graded **must be submitted in writing** with your exam to the TA at the end of
section that day._ No regrade requests will be allowed afterwards regardless of
whether or not you attended section. Please get in touch with me if you
anticipate missing section due to a family emergency or a medical reason.


## Grading

_Grading method:_ Homework assignments, midterm exams, and Final exam.

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                             |
|:---------------------------------------------------------------------------:|
| 25% Assignments + 20% Midterm 1 + 20% Midterm 2 + 35% Final Exam            |
| or                                                                          |
| 25% Assignments + 30% Highest Midterm + 44% Final Exam[^one]                |

[^one]: Please notice that the weights in this breakdown do not add up to 100%.

_Letter grades:_  

|                       |                      |                    |
|:----------------------|:---------------------|:-------------------|
|                       | A (93.33% or higher) | A- (90% -- 93.32%) |
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |

The remaining grades will be assigned at my own discretion. Please DO NOT
request exceptions.

**All grades are final when filed by the instructor on the Final Grade Report.**

## Policies and procedures

Once a homework assignment is posted (CCLE) you will have at least one week to
complete it. The actual due date might change but it will always be available
in the description of the assignment. You are encouraged to check CCLE on a
regular basis to find out about changes. You will upload the requested source
files to our course's CCLE website before the date and time posted on CCLE. The
assignments will be due at the time the PIC Lab closes on the due date. There
will be about 7, or 8, homework assignments throughout the quarter. No late
homework will be accepted for any reason, but **your lowest homework score will
be dropped**. _In an effort to be fair to all students, messages sent to my
email address that contain either `.cpp`, `.hpp`, `.txt`, or  `.h` attachments
will automatically be deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submit them through the proper channels. The 'official' compiler we
will use is the one available at the computers in the PIC Lab (Visual Studio
2017 as of this writing). If your code does not compile against it, you will
receive an initial score of 0/20 points. To receive credit for your work, you
must submit a regrade request to [pic10b.grader@gmail.com][regrade] within 7
days of the date you are notified your project failed to compile. Please
indicate in your request what changes are needed in your file(s) so that your
assignment no longer fails to compile. However, be aware that the following
rules apply:

 i. If _the fix_ is described within the body of the email, and if it is not
    _too long_. The maximum possible deduction is 1 point. 

    > _E.g._ Dear reader, please add `#include <string>` to the top (line 1) of
    > my `hw2.cpp` file.

 i. If the changes needed to make the project compile are significant, a _new
    version_ of the project can be sent as an attachment to the reader.
    However a minimum penalty of 2 points will be assesed to the overall score.
 
    > _E.g._ Dear reader, I mistakenly submitted an incomplete version of my
    > third assignment. Please replace my submission with the attached file.

 i. Working projects submitted by other students will be graded before ones
    that have _been fixed_. In practice, this likely means that you will not
    get to find out your score until up to 4 or 5 weeks after grades are
    reported to MyUCLA.
 
To avoid potential unnecessary delays and/or penalties, you are encouraged to
test your code in the PIC lab before the assignment is due.

[regrade]: mailto:pic10b.grader@gmail.com

As pointed out before, scores on homework assignments and exams will appear on
MyUCLA. It is your responsibility to verify in a timely manner that the scores
appearing there are accurate. If you believe your homework has been graded
incorrectly, or that your score was not correctly recorded, you must bring this
to the attention of the instructor as soon as possible. I reserve the right to
asses a _tardy_ penalty on requests that are not made within 7 calendar days of
the date the scores were recorded.

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (i.e., general ideas and
words are OK but code is NOT OK). If you need specific help with your programs
you may consult the TA or the instructor only. Do not copy or cite any part of
a program or document written by someone else (e.g., code found online).
Homework solutions are automatically monitored for copied code.


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: http://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

